function execute_cluster_analysis()
    load kmeansdata;
    [IDX_kmeans_VRC, C_kmeans_VRC, kmeans_vrc, nr_clusters_kmeans_VRC] = validateKmeansVRC('sqEuclidean', X);
    plot2DClusters(X, IDX_kmeans_VRC, C_kmeans_VRC, nr_clusters_kmeans_VRC, 'kmeans-VRC');
    [IDX_kmeans_silhouette, C_kmeans_silhouette, kmeans_silhouette, nr_clusters_kmeans_silhouette] = validateKmeansSilhouette('sqEuclidean', X);
    plot2DClusters(X, IDX_kmeans_silhouette, C_kmeans_silhouette, nr_clusters_kmeans_silhouette, 'kmeans-silhouette');
    [IDX_linkage_VRC, C_linkage_VRC, linkage_VRC, nr_clusters_linkage_VRC] = validateSingleLinkageVRC('euclidean', X);
    plot2DClusters(X, IDX_linkage_VRC, C_linkage_VRC, nr_clusters_linkage_VRC, 'single-linkage-VRC');
    [IDX_linkage_silhouette, C_linkage_silhouette, linkage_silhouette, nr_clusters_linkage_silhouette] = validateSingleLinkageSilhouette('euclidean', X);
    plot2DClusters(X, IDX_linkage_silhouette, C_linkage_silhouette, nr_clusters_linkage_silhouette, 'single-linkage-silhouette');
end