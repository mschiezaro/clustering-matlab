% Calcula o Kmeans usando a descri��o de dist�ncia passada por par�metro,
% tenta encontrar a melhor parti��o com 2 at� sqrt(size(data)) clusters. A
% melhor parti��o � com o valor maior dado pelo �ndice VRC
% distance -  descri��o do tipo de dist�ncia (ver help kmeans)
% data - dados a serem analisados
% return 
% IDX_ret Indica em que cluster cada dado est�
% C_ret Centroides dos clusters
% VRC - �ndice de valida��o calculado
% nr_clusters - N�mero de clusters da melhor parti��o
function [IDX_ret, C_ret, vrc_ret, nr_clusters] = validateKmeansVRC(distance, data) 

    % recupera a quantidade de dados para e estimarmos que teremos no m�ximo
    % sqrt(tamanho dados) de clusters
    [length, y] = size(data);
    clusters = round(sqrt(length));
    best_partition = 0;
    
    % quantidade de dados � pequena retorna 0
    if clusters < 2
        IDX_ret = 0;
        C_ret = 0;
        vrc_ret = 0;
    else
        %  Executa o Kmeans 10 vezes (pegando aleatoriamente o ponto
        %  inicial) com 2 at� sqrt(N) e verifica qual � melhor crit�rio de
        %  valida��o dos clusters a partir do silhouette
        for  i=2:clusters
            
            [IDX,C,sumd,D] = kmeans(data, i, 'replicates', 10, 'distance', distance);
            
            vrc = calculateVRC(data, IDX, C, i);
            % � maior que a anterior
            if vrc > best_partition
                IDX_ret = IDX;
                C_ret = C;
                vrc_ret = vrc;
                best_partition = vrc;
            end
        end
    end
    nr_clusters = max(IDX_ret);
    
end