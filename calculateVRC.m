% Calcula o �ndice de valida��o Calinski-Harabasz (VRC)
% data - Matriz onde cada linha � um objeto e cada coluna � uma dimens�o
% (feature)
% cluster_centroid - Matriz N x 1 cada linha armazena o centr�ide de seu
% respectivo cluster
% nr_cluster N�mero de clusters dos dados
% F�rmula trace(B)/trace(W) * (N-k)/(k-1)
%
function VRC = calculateVRC(data, index, cluster_centroid, nr_clusters)
    
    % calcula o centr�ide do conjunto total dos dados 
    global_centroid = mean(data);
    % tamnho do conjunto de dados
    [N, not_used] = size(data);

    % Calcula o W
    W = 0;
    for i=1:nr_clusters
        % Para o cluster i, verifica quais dados fazem parte do mesmo
        idx = index==i;
        data_cluster = data(idx,:);

        % verifica o tamanho do cluster
        [length, not_used] = size(data_cluster);   
        %armazena a quantidade de elementos em cada cluster
        cluster_size(i,1) = length;
        %Calcula W
        for j=1:length
            subtract = data_cluster(j,:) - cluster_centroid(i,:);            
            W = W + subtract * subtract';
        end
    end
    % Calcula o B
    [length, not_used] = size(cluster_centroid);        
    B = 0;
    for i=1:length
        subtract = cluster_centroid(i,:) - global_centroid;
        B = B + (subtract * subtract')* cluster_size(i,1);        
    end
    % calula o VRC
    VRC = (B/W) *((N - nr_clusters)/(nr_clusters - 1));
end