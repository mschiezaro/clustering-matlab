% Calcula single linkage usando a descri��o de dist�ncia passada por par�metro,
% tenta encontrar a melhor parti��o com 2 at� sqrt(size(data)) clusters. A
% melhor parti��o � com o valor maior dado pelo �ndice silhouette
% distance -  descri��o do tipo de dist�ncia (ver help pdist)
% data - dados a serem analisados
% return 
% IDX_ret Indica em que cluster cada dado est�
% C_ret Centroides dos clusters
% silh_ret - �ndice de valida��o calculado
% nr_clusters - N�mero de clusters da melhor parti��o
function [IDX_ret, C_ret, silh_ret, nr_clusters] = validateSingleLinkageSilhouette(distance, data) 

    % recupera a quantidade de dados para e estimarmos que teremos no m�ximo
    % sqrt(tamanho dados) de clusters
    [length, y] = size(data);
    clusters = round(sqrt(length));
    best_partition = 0;
    
    % quantidade de dados � pequena retorna 0
    if clusters < 2
        IDX_ret = 0;
        C_ret = 0;
        silh_ret = 0;
    else
        %  Executa single linkage de 2 at� sqrt(N) clusters e verifica qual
        % � melhor crit�rio de valida��o dos clusters a partir do silhouette
        for  i=2:clusters
          
            p_dist = pdist(data, distance);
            Zdist = linkage(p_dist, 'single');
            IDX = cluster(Zdist,'maxclust',i);
            for j=1:i
                centroid(j,:) = mean(data(IDX==j,:));
            end
            [silh, h] = silhouette(data,IDX);
            meas = mean(silh);
            % � maior que a anterior
            if meas > best_partition
                IDX_ret = IDX;
                C_ret = centroid;
                silh_ret = silh;
                best_partition = meas;
                saveas(h, 'single-linkage-silhouette-plot.jpg');
            end
        end
    end
    nr_clusters = max(IDX_ret);
end