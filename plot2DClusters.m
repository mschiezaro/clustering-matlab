function plot2DClusters(data, cluster, centroid, nr_clusters, file_prefix_name)
    [size_data, size_features] = size(data);

    leg ={};
    leg_index = 1;
    colors = {'r.', 'g.', 'b.', 'c.', 'm.', 'y.', 'k.', 'r^', 'g^', 'b^', 'c^', 'm^', 'y^', 'k^', 'r*', 'g*', 'b*', 'c*', 'm*', 'y*', 'k*'};
    [unused, max_colors_index] = size(colors);
    colors_index = 1;
    for j=1:size_features
        for l=j+1:size_features
            for i=1:nr_clusters                
                if colors_index > max_colors_index
                    colors_index = 1;
                end
                h = plot(data(cluster==i,j),data(cluster==i,l),colors{colors_index},'MarkerSize',12);        
                colors_index = colors_index + 1;
                leg{leg_index} = strcat('Cluster', num2str(leg_index));
                leg_index = leg_index + 1;
                if(i == 1)
                    hold on;
                end
            end
            legend(leg);            
            xlabel(strcat('Feature',num2str(j))); 
            ylabel(strcat('Feature',num2str(l)))
            plot(centroid(:,j),centroid(:,l),'kx', 'MarkerSize',12,'LineWidth',2);            
            h = plot(centroid(:,j),centroid(:,l),'ko', 'MarkerSize',12,'LineWidth',2);            
            saveas(h, strcat(file_prefix_name, num2str(j),num2str(l), '.jpg'));
            leg_index = 1;
            colors_index = 1;
            hold off;
        end
    end
end
